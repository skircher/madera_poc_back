
# PHASE DEVELOPPEMENT

#Dossier de travail
1)Créer dossier comportant projet Madera : ../madera

#Partie Backend 
1) Cloner "backend" dans dossier backend :	git clone https://gitlab.com/skircher/madera_poc_back.git	:	../madera/backend
2) Installer SDK 11
3) ouvrir le projet avec son Ide et lancer le projet


#Partie Frontend
1) Cloner "frontend" dans dosser frontend : git clone https://gitlab.com/skircher/madera_front.git		:	../madera/frontend
2) Dans le dossier "frontend" installer package : npm install
3) Dans le dossier "frontend" lancer le frontend : npm start


#Partie Editor
1) Cloner "three.js editor" dans dossier "editor" : git clone https://github.com/mrdoob/three.js.git   : ../madera/editor
2) Cloner "editor three.js personalisé" :  git clone https://gitlab.com/skircher/mader_editor.git
3) remplacer dossier "editor" du repo "three.js editor" par le contenu du repo "editor three.js personalisé"		: ../madera/editor/editor
4) Récupérer dossiers "blender" et "threejs" concernant les modules de configuration des maisons : https://drive.google.com/drive/u/0/folders/1mcxK8iL8jGk1LsoNxvKV0tzFnvXzB4KM
5) Créer dans repo "three.js editor" un dossier "modelisation" et insérer les dossiers "blender" et "threejs"		: ../madera/editor/modelisation
6) Dans dossier "../madera/editor/editor" lancer three js editor : npm i && npm start

#Serveur Redis
1) Télécharger Redis (redis-2.4.5-win32-win64.zip\64bit) : https://github.com/dmajkic/redis/downloads
2) Lancer redis-2.4.5-win32-win64.zip\64bit\redis-server.exe


#Serveur SMTP
1) Installer fake-smtp-server : npm install -g fake-smtp-server
2) Lancer fake-smtp-server : fake-smtp-server

# PHASE DEPLOIEMENT

#Server NGINX
1) Installer serveur NGINX
2) Configurer fichier "nginx.conf" : insérer certificat SSL, régler location de l'appli, et url de l'appli
3) Lancer serveur NGINX : nginx start

#Partie Frontend
1)Configurer url serveur dans fichier .env.production
2)Dans le dossier Frontend, Build partie Front : npm run-script build

#Partie Backend 
1) Configurer url serveur dans application-prod.properties
2) Dans "application.properties" :spring.profiles.active=prod
3) Lancer serveur redis : redis-2.4.5-win32-win64.zip\64bit\redis-server.exe
4) Lancer serveur backend 

#Partie Editor
1) Dans dossier "../madera/editor/editor" lancer three js editor : npm i && npm start

