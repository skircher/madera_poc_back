CREATE TABLE produits (
    id serial PRIMARY KEY,
    nom VARCHAR ( 50 ) UNIQUE NOT NULL,
    date_creation TIMESTAMP,
    date_maj TIMESTAMP,
    prix_total INTEGER,
    validation BOOLEAN
)