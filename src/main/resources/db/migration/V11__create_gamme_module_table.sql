CREATE TABLE gamme_modules (
    id serial PRIMARY KEY,
    gamme_id INT,
    FOREIGN KEY (gamme_id) REFERENCES GAMMES ON DELETE CASCADE,
    module_client_id INT,
    FOREIGN KEY (module_client_id) REFERENCES MODULE_CLIENTS ON DELETE CASCADE
)