CREATE TABLE villes (
    id serial PRIMARY KEY,
    nom VARCHAR ( 50 ) NOT NULL,
    code_postal VARCHAR ( 50 ) NOT NULL,
    pays VARCHAR ( 50 )
)