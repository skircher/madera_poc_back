CREATE TABLE etape_compose_devis (
    id serial PRIMARY KEY,
    etape_id INT,
    FOREIGN KEY (etape_id) REFERENCES ETAPES ON DELETE CASCADE,
    devis_id INT,
    FOREIGN KEY (devis_id) REFERENCES DEVIS ON DELETE CASCADE,
    pourcentage INT
   )