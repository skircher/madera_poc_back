CREATE TABLE situer_clients (
    id serial PRIMARY KEY,
    client_id INT,
    FOREIGN KEY (client_id) REFERENCES CLIENTS ON DELETE CASCADE,
    adresse_id INT,
    FOREIGN KEY (adresse_id) REFERENCES ADRESSES ON DELETE CASCADE
)