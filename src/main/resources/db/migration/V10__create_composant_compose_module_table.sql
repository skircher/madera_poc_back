CREATE TABLE composant_compose_modules (
    id serial PRIMARY KEY,
    module_client_id INT,
    FOREIGN KEY (module_client_id) REFERENCES MODULE_CLIENTS ON DELETE CASCADE,
    composant_id INT,
    FOREIGN KEY (composant_id) REFERENCES COMPOSANTS ON DELETE CASCADE,
    quantite INTEGER NOT NULL
)