CREATE TABLE module_compose_produits (
    id serial PRIMARY KEY,
    module_client_id INT,
    FOREIGN KEY (module_client_id) REFERENCES MODULE_CLIENTS ON DELETE CASCADE,
    produit_id INT,
    FOREIGN KEY (produit_id) REFERENCES PRODUITS ON DELETE CASCADE
)