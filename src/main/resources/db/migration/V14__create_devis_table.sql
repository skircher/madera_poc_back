CREATE TABLE devis (
    id serial PRIMARY KEY,
    numero_devis INTEGER NOT NULL,
    date_creation TIMESTAMP,
    date_maj TIMESTAMP,
    etat VARCHAR ( 25 ),
    etape_id INT,
    FOREIGN KEY (etape_id) REFERENCES ETAPES ON DELETE CASCADE
)