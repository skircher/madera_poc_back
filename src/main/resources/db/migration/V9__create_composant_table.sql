CREATE TABLE composants (
    id serial PRIMARY KEY,
    nom VARCHAR ( 50 ) NOT NULL,
    dimension NUMERIC[],
    nature VARCHAR ( 50 ) NOT NULL,
    famille VARCHAR ( 50 ) NOT NULL,
    quantite_stock INTEGER NOT NULL,
    prix numeric NOT NULL
)