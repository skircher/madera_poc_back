CREATE TABLE adresses (
    id serial PRIMARY KEY,
    nom_adr VARCHAR ( 50 ) NOT NULL,
    rue VARCHAR ( 150 ) NOT NULL,
    numero INTEGER NOT NULL,
    complement VARCHAR ( 50 )
)