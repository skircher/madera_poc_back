CREATE TABLE clients (
    id serial PRIMARY KEY,
    prenom VARCHAR ( 50 ) UNIQUE NOT NULL,
    nom VARCHAR ( 50 ) UNIQUE NOT NULL,
    email VARCHAR ( 100 ) UNIQUE NOT NULL,
    telephone_fixe VARCHAR ( 10 ),
    telephone_port VARCHAR( 10 ),
    genre BOOLEAN,
    date_creation TIMESTAMP
)