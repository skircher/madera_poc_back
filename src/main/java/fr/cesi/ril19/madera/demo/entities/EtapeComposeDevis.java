package fr.cesi.ril19.madera.demo.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="etape_compose_devis")
@IdClass(EtapeComposeDevisId.class)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EtapeComposeDevis {
    @Id
    @ManyToOne
    @JoinColumn(name = "etape_id", referencedColumnName = "id")
    private Etape etape;

    @Id
    @ManyToOne
    @JoinColumn(name = "devis_id", referencedColumnName = "id")
    @JsonBackReference
    private Devis devis;

    private Integer pourcentage;

}
