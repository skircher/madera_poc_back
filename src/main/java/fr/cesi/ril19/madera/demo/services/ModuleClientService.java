package fr.cesi.ril19.madera.demo.services;

import fr.cesi.ril19.madera.demo.entities.Composant;
import fr.cesi.ril19.madera.demo.entities.FamilleModule;
import fr.cesi.ril19.madera.demo.entities.Gamme;
import fr.cesi.ril19.madera.demo.entities.ModuleClient;
import fr.cesi.ril19.madera.demo.repositories.ModuleClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ModuleClientService {

    ModuleClientRepository moduleClientRepo;
    FamilleModuleService familleModuleService;
    ComposantService composantService;
    GammeService gammeService;

    @Autowired
    ModuleClientService(ModuleClientRepository moduleClientRepo, FamilleModuleService familleModuleService, ComposantService composantService, GammeService gammeService){
        this.moduleClientRepo = moduleClientRepo;
        this.familleModuleService = familleModuleService;
        this.composantService = composantService;
        this.gammeService = gammeService;
    }

    /**
     *
     * @return list of module clients
     */
    public List<ModuleClient> getModuleClients() {
        return (List<ModuleClient>) this.moduleClientRepo.findAll();
    }

    /**
     *
     * @param id
     * @return module client thanks to Id
     */
    public ModuleClient findModuleClientById(Long id){
        return this.moduleClientRepo.findById(id).orElseThrow();
    }


    /**
     *
     * @param nom
     * @return module client thanks to nom
     */
    public ModuleClient findModuleClientByName(String nom){
        return this.moduleClientRepo.moduleByName(nom);
    }

    /**
     *
     * @param noms
     * @return module client thanks to nom
     */
    public List<ModuleClient> getModuleClientsByName(List<String> noms){
        List<ModuleClient> listM = new ArrayList<>();
        for (String nom: noms) {
            listM.add(findModuleClientByName(nom));
        }
        return listM;
    }
    /**
     *
     * @param m
     * @return module cliebt save
     */
    public ModuleClient saveModuleClient(ModuleClient m) {
        return this.moduleClientRepo.save(m);
    }



    public List<ModuleClient> importModuleClient(MultipartFile file) throws IOException {
        String separator = ",";
        InputStream is = file.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<ModuleClient> listM = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            String[] array = line.split(separator);
            ModuleClient m = new ModuleClient();
            //v.setId(Long.parseLong(array[0]));
            m.setNom(array[0].replace("\"",""));
            m.setFamilleModule(this.familleModuleService.findFamilleModuleById(Long.parseLong(array[1])));
            List<Composant> listComp = new ArrayList<>();
            for (String component:
                 array[2].replace("\"","").split(";")) {
                System.out.print(component);
                Composant compo = this.composantService.findComposantById(Long.parseLong(component));
                listComp.add(compo);
            }
            m.setComposants(listComp);
            List<Gamme> listGam = new ArrayList<>();
            for (String gamme:
                    array[3].replace("\"","").split(";")) {
                System.out.print(gamme);
                Gamme gam = this.gammeService.findGammeById(Long.parseLong(gamme));
                listGam.add(gam);
            }
            m.setGammes(listGam);
            listM.add(m);
        }return listM;
    }

    public double getPrixModule(Long id){
        double prix = 0;
        ModuleClient moduleClient = this.findModuleClientById(id);
        for(Composant composant:moduleClient.getComposants()){
            prix = prix + composant.getPrix();
        }
        return prix;
    }

}
