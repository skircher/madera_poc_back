package fr.cesi.ril19.madera.demo.repositories;

import fr.cesi.ril19.madera.demo.entities.ModuleClient;
import fr.cesi.ril19.madera.demo.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, String> {
    User findByUsername(String username);

    @Query(value="SELECT * FROM users " +
            "WHERE username LIKE %:nom%"
            , nativeQuery = true)
    User UserByName(String nom);
}
