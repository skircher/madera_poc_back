package fr.cesi.ril19.madera.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="famille_modules")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FamilleModule {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String nom;
}
