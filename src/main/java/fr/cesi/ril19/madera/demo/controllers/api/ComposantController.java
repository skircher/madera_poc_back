package fr.cesi.ril19.madera.demo.controllers.api;

import fr.cesi.ril19.madera.demo.entities.Composant;
import fr.cesi.ril19.madera.demo.services.ComposantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/composant")
public class ComposantController {
    private ComposantService composantService;
    @Autowired
    ComposantController(ComposantService composantService){
        this.composantService = composantService;
    }

    /**
     * GET /composant
     * @return
     */
    @RequestMapping("")
    public List<Composant> getComposants() {
        return this.composantService.getComposants();
    }

    /**
     * GET /composant/1 - /composant/id
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public Composant getComposantById(@PathVariable Long id) {
        return this.composantService.findComposantById(id);
    }

    /**
     * POST /produit
     * @param c
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Composant> postComposant(@RequestBody Composant c) {
        return new ResponseEntity<Composant>(this.composantService.saveComposant(c), HttpStatus.CREATED);
    }

    /**
     * POST /import
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<List<Composant>> importComposants(@RequestParam("file") MultipartFile file) throws IOException {
        for (Composant c:this.composantService.importComposant(file)) {   //allows to identify if manager doesn't exist in table Users
            this.composantService.saveComposant(c);
        }
        return new ResponseEntity<List<Composant>>(this.composantService.getComposants(), HttpStatus.CREATED);
    }
}
