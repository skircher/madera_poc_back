package fr.cesi.ril19.madera.demo.controllers.api;

import com.itextpdf.text.BadElementException;
import fr.cesi.ril19.madera.demo.entities.Devis;
import fr.cesi.ril19.madera.demo.entities.Produit;
import fr.cesi.ril19.madera.demo.payload.GeneratePdfReport;
import fr.cesi.ril19.madera.demo.services.DevisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/devis")
public class DevisController {
    private DevisService devisService;
    private GeneratePdfReport generatePdfReport;

    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    DevisController( DevisService devisService, GeneratePdfReport generatePdfReport, RedisTemplate<String, String> redisTemplate){
        this.devisService = devisService;
        this.generatePdfReport = generatePdfReport;
        this.redisTemplate = redisTemplate;
    }

    /**
     * GET /devis
     * @return
     */
    @RequestMapping("")
    public List<Devis> getDeviss() {
        return this.devisService.getDeviss();
    }

    /**
     * GET /devis
     * @return
     */
    @RequestMapping("/{id}")
    public Devis getDevisById(@PathVariable Long id) {
        return this.devisService.findDevisById(id);
    }

    /**
     * POST /devis
     * @param g
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Devis> postDevis(@RequestBody Devis g) {
        return new ResponseEntity<Devis>(this.devisService.saveDevis(g), HttpStatus.CREATED);
    }

    /**
     * PUT /devis
     * @param d
     * @return
     */
    @PutMapping("")
    public ResponseEntity<Devis> putDevis(@RequestBody Devis d) {
        return new ResponseEntity<Devis>(this.devisService.updateDevis(d), HttpStatus.CREATED);
    }

    /**
     * POST /import
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<List<Devis>> importDeviss(@RequestParam("file") MultipartFile file) throws IOException {
        for (Devis g:this.devisService.importDevis(file)) {
            this.devisService.saveDevis(g);
        }
        return new ResponseEntity<List<Devis>>(this.devisService.getDeviss(), HttpStatus.CREATED);
    }

    /**
     * GET /devis/client_id
     * @return
     */
    @RequestMapping("/client/{client_id}")
    public List<Devis> getDevisByClient_id(@PathVariable Long client_id) {
        return this.devisService.getDevisByClient(client_id);
    }

    /**
     * GET /pdfreport/{id}/produit/{id_produit}
     * @param id, id_produit
     * @return devis pdf
     */
    @RequestMapping(value = "/pdfreport/{id}/produit/{id_produit}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> devisReport(@PathVariable Long id, @PathVariable Long id_produit) throws IOException, BadElementException {

        Devis devis = devisService.findDevisById(id);

        ByteArrayInputStream bis = this.generatePdfReport.docTechniquePdf(devis,id_produit);

        var headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=devisreport.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
}
