package fr.cesi.ril19.madera.demo.repositories;


import fr.cesi.ril19.madera.demo.entities.ModuleClient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModuleClientRepository
        extends CrudRepository<ModuleClient, Long> {

    @Query(value="SELECT * FROM module_clients " +
            "WHERE nom LIKE %:nom%"
            , nativeQuery = true)
    ModuleClient moduleByName(String nom);
}
