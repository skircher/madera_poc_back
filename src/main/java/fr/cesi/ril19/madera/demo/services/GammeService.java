package fr.cesi.ril19.madera.demo.services;

import fr.cesi.ril19.madera.demo.entities.Gamme;
import fr.cesi.ril19.madera.demo.repositories.GammeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class GammeService {

    GammeRepository gammeRepo;

    @Autowired
    GammeService(GammeRepository gammeRepo){
        this.gammeRepo = gammeRepo;
    }

    /**
     *
     * @return list of gammes
     */
    public List<Gamme> getGammes() {
        return (List<Gamme>) this.gammeRepo.findAll();
    }

    /**
     *
     * @param id
     * @return project thanks to Id
     */
    public Gamme findGammeById(Long id) {
        return this.gammeRepo.findById(id).orElseThrow();
    }

    /**
     *
     * @param g
     * @return gamme save
     */
    public Gamme saveGamme(Gamme g) {
        return this.gammeRepo.save(g);
    }

    /**
     * Import Gamme in csv file
     * @param file csv
     * @return List of Gamme present in csv file
     */
    public List<Gamme> importGamme(MultipartFile file) throws IOException {
        String separator = ",";
        InputStream is = file.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<Gamme> listG = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            String[] array = line.split(separator);
            Gamme g = new Gamme();
            //v.setId(Long.parseLong(array[0]));
            g.setNom(array[0]);
            listG.add(g);
        }return listG;
    }

}

