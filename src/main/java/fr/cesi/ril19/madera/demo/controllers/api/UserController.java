package fr.cesi.ril19.madera.demo.controllers.api;

import fr.cesi.ril19.madera.demo.entities.Produit;
import fr.cesi.ril19.madera.demo.entities.User;
import fr.cesi.ril19.madera.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/user")
@RestController
public class UserController {

    private UserService userService;
    @Autowired
    UserController(UserService userService){
        this.userService = userService;
    }
    /**
     * POST /user
     * @param u
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Long> postUser(@RequestBody User u) {
        return new ResponseEntity<Long>( this.userService.saveDto(u), HttpStatus.CREATED);
    }
}
