package fr.cesi.ril19.madera.demo.repositories;

import fr.cesi.ril19.madera.demo.entities.Adresse;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdresseRepository extends CrudRepository<Adresse, Long> {
}
