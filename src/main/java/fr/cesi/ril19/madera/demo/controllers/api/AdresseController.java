package fr.cesi.ril19.madera.demo.controllers.api;

import fr.cesi.ril19.madera.demo.entities.Adresse;

import fr.cesi.ril19.madera.demo.services.AdresseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/adresse")
public class AdresseController {

    private AdresseService adresseService;

    @Autowired
    AdresseController(AdresseService adresseService){
        this.adresseService = adresseService;
    }

    /**
     * GET /adresse
     * @return
     */
    @RequestMapping("")
    public List<Adresse> getAdresses() {
        return this.adresseService.getAdresses();
    }

    /**
     * GET /adresse/1 - /adresse/id
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    @ResponseBody
    public Adresse getAdresseById(@PathVariable Long id) { return this.adresseService.findAdresseById(id); }

    /**
     * POST /adresse
     * @param a
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Adresse> postAdresse(@RequestBody Adresse a)  {
        return new ResponseEntity<Adresse>(this.adresseService.saveAdresse(a), HttpStatus.CREATED);
    }

    /**
     * POST /import
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<List<Adresse>> importAdresses(@RequestParam("file") MultipartFile file) throws IOException {
        for (Adresse a:this.adresseService.importAdresse(file)) {   //allows to identify if manager doesn't exist in table Users
            this.adresseService.saveAdresse(a);
        }
        return new ResponseEntity<List<Adresse>>(this.adresseService.getAdresses(), HttpStatus.CREATED);
    }
}
