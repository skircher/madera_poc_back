package fr.cesi.ril19.madera.demo.controllers.api;

import fr.cesi.ril19.madera.demo.entities.Etape;
import fr.cesi.ril19.madera.demo.entities.FamilleModule;
import fr.cesi.ril19.madera.demo.services.EtapeService;
import fr.cesi.ril19.madera.demo.services.FamilleModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/etape")
public class EtapeController {
    private EtapeService etapeService;

    @Autowired
    EtapeController( EtapeService etapeService ){
        this.etapeService = etapeService;
    }

    /**
     * GET /etape
     * @return
     */
    @RequestMapping("")
    public List<Etape> getEtapes() {
        return this.etapeService.getEtapes();
    }

    /**
     * POST /import
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<List<Etape>> importEtapes(@RequestParam("file") MultipartFile file) throws IOException {
        for (Etape e:this.etapeService.importEtape(file)) {
            this.etapeService.saveEtape(e);
        }
        return new ResponseEntity<List<Etape>>(this.etapeService.getEtapes(), HttpStatus.CREATED);
    }
}
