package fr.cesi.ril19.madera.demo.services;

import fr.cesi.ril19.madera.demo.entities.Adresse;
import fr.cesi.ril19.madera.demo.entities.Client;
import fr.cesi.ril19.madera.demo.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class ClientService {

    ClientRepository clientRepo;
    AdresseService adresseService;

    @Autowired
    ClientService(ClientRepository clientRepo, AdresseService adresseService) {
        this.clientRepo = clientRepo;
        this.adresseService = adresseService;
    }

    /**
     *
     * @return Liste de clients
     */
    public List<Client> getClients() {
        return (List<Client>) this.clientRepo.findAll();
    }

    /**
     *
     * @param id
     * @return client grace à l'id du client
     */
    public Client findClientById(Long id) {
        return this.clientRepo.findById(id).orElseThrow();
    }

    /**
     *
     * @param c
     * @return Client save
     */
    public Client saveClient(Client c) {
        List<Adresse> listAdr = new ArrayList<>();
        for (Adresse a:
                c.getAdresses()) {
            Adresse adr = this.adresseService.findAdresseById(a.getId());
            listAdr.add(adr);
        }
        c.setAdresses(listAdr);
        c.setDate_creation(new Date());
        return this.clientRepo.save(c);
    }

    public Client addAdresseClient(Long id, Adresse a){
        Client c = findClientById(id);
        List<Adresse> listAdr = c.getAdresses();
        listAdr.add(a);
        c.setAdresses(listAdr);
        return this.clientRepo.save(c);
    }

    public List<Client> importClient(MultipartFile file) throws IOException {
        String separator = ",";
        InputStream is = file.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<Client> listC = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            String[] array = line.split(separator);
            Client c = new Client();
            //v.setId(Long.parseLong(array[0]));
            c.setPrenom(array[0].replace("\"",""));
            c.setNom(array[1]);
            c.setEmail(array[2]);
            c.setTelephone_fixe(array[3]);
            c.setTelephone_port(array[4]);
            c.setGenre(Boolean.parseBoolean(array[5]));
            List<Adresse> listAdr = new ArrayList<>();
            for (String adresse:
                    array[6].replace("\"","").split(";")) {
                System.out.print(adresse);
                Adresse adr = this.adresseService.findAdresseById(Long.parseLong(adresse));
                listAdr.add(adr);
            }
            c.setAdresses(listAdr);
            listC.add(c);
        }return listC;
    }

}
