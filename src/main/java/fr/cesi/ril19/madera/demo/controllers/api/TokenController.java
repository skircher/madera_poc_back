package fr.cesi.ril19.madera.demo.controllers.api;

import fr.cesi.ril19.madera.demo.configuration.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;


@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/token")
@RestController
public class TokenController {

    private RedisTemplate<String, String> redisTemplate;
    private SecurityConstants securityConstants = new SecurityConstants();

    @Autowired
    TokenController(RedisTemplate<String, String> redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    /**
     * POST /
     * @param token
     * @return
     */
    @PostMapping("")
    public String saveTokenRedis(@RequestBody String token) {
        String keyToken = UUID.randomUUID().toString();
        System.out.println(keyToken);
        redisTemplate.opsForValue().set(keyToken,securityConstants.TOKEN_PREFIX+token);
        return keyToken;
    }

    /**
     * GET /
     * @param keyToken
     * @return
     */
    @GetMapping("/{keyToken}")
    public String getToken(@PathVariable String keyToken) {
        String token = redisTemplate.opsForValue().get(keyToken);
        redisTemplate.delete(keyToken);
        return token;
    }
}
