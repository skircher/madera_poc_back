package fr.cesi.ril19.madera.demo.controllers.api;

import fr.cesi.ril19.madera.demo.entities.Ville;
import fr.cesi.ril19.madera.demo.services.VilleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/ville")
public class VilleController {

    private VilleService villeService;

    @Autowired
    VilleController(VilleService villeService){
        this.villeService = villeService;
    }


    /**
     * GET /ville
     * @return
     */
    @RequestMapping("")
    public List<Ville> getVilles() {
        return this.villeService.getVilles();
    }

    /**
     * GET /ville/1 - /ville/id
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    @ResponseBody
    public Ville getVilleById(@PathVariable Long id) { return this.villeService.findVilleById(id); }

    /**
     * POST /ville
     * @param v
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Ville> postVille(@RequestBody Ville v)  {
        return new ResponseEntity<Ville>(this.villeService.saveVille(v), HttpStatus.CREATED);
    }

    /**
     * POST /import
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<List<Ville>> importVilles(@RequestParam("file") MultipartFile file) throws IOException {
        for (Ville v:this.villeService.importVille(file)) {   //allows to identify if manager doesn't exist in table Users
            this.villeService.saveVille(v);
        }
        return new ResponseEntity<List<Ville>>(this.villeService.getVilles(), HttpStatus.CREATED);
    }


}
