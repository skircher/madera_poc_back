package fr.cesi.ril19.madera.demo.controllers.api;

import com.itextpdf.text.BadElementException;
import fr.cesi.ril19.madera.demo.services.EmailService;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;

@CrossOrigin(origins = "*")
@RequestMapping("/api/v1")
@RestController
public class EmailController {

    private final EmailService emailService;

    public EmailController(EmailService emailService) {
        this.emailService = emailService;
    }

    @RequestMapping("/mails_validation_devis/{username}/devis/{devisId}/produit/{produitId}")
    public void envoiCourriersValidationDevis(@PathVariable String username, @PathVariable Long devisId, @PathVariable Long produitId) throws MessagingException, IOException, BadElementException {
        System.out.println("user : " + username);
        System.out.println("devis id : " + devisId);
        this.emailService.envoiCourrier(username,devisId,produitId);
    }

}
