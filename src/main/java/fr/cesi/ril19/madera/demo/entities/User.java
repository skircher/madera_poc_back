package fr.cesi.ril19.madera.demo.entities;

import lombok.AllArgsConstructor;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;

@Entity
@Data
@DiscriminatorValue("USER")
@Table(name="users")
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    private String first_name;

    private String last_name;

    private String email;

    public <E> User(String username, String password, ArrayList<E> es) {
    }
}
