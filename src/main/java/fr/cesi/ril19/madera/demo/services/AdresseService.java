package fr.cesi.ril19.madera.demo.services;

import fr.cesi.ril19.madera.demo.entities.Adresse;
import fr.cesi.ril19.madera.demo.repositories.AdresseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class AdresseService {
    AdresseRepository adressetRepo;
    VilleService villeService;

    @Autowired
    AdresseService(AdresseRepository adressetRepo, VilleService villeService){

        this.adressetRepo = adressetRepo;
        this.villeService = villeService;
    }

    /**
     *
     * @return liste des adresses
     */
    public List<Adresse> getAdresses() {
        return (List<Adresse>) this.adressetRepo.findAll();
    }

    /**
     *
     * @param id
     * @return adresse grace à Id
     */
    public Adresse findAdresseById(Long id) {return this.adressetRepo.findById(id).orElseThrow(); }

    /**
     *
     * @param a
     * @return Adresse save
     */
    public Adresse saveAdresse(Adresse a) {
        return this.adressetRepo.save(a);
    }

    public List<Adresse> importAdresse(MultipartFile file) throws IOException {
        String separator = ",";
        InputStream is = file.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<Adresse> listA = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            Integer villeId = new Random().nextInt(304)+1;
            String[] array = line.split(separator);
            Adresse a = new Adresse();
            //v.setId(Long.parseLong(array[0]));
            a.setNom_adr(array[0]);
            //a.setVille((long)villeId);
            a.setRue(array[1]);
            a.setNumero(Integer.parseInt(array[2]));
            a.setComplement(array[3]);
            a.setVille(this.villeService.findVilleById((long)villeId));

            listA.add(a);
        }return listA;
    }

}
