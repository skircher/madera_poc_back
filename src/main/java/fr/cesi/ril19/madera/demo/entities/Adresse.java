package fr.cesi.ril19.madera.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="adresses")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Adresse {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String nom_adr;
    private String rue;
    private Integer numero;
    private String complement;

    @ManyToOne
    @JoinColumn(name="ville_id")
    private Ville ville;
}
