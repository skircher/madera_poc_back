package fr.cesi.ril19.madera.demo.services;

import fr.cesi.ril19.madera.demo.entities.UtilisateurPrincipal;
import fr.cesi.ril19.madera.demo.entities.User;
import fr.cesi.ril19.madera.demo.repositories.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserService implements UserDetailsService {


    UserRepository userRepo;
    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    UserService(UserRepository userRepository){
        this.userRepo = userRepository;
    }

    public User saveUser(User u)  { return this.userRepo.save(u);}

    public User findUserById(String id) {
        return this.userRepo.findById(id).orElseThrow();
    }

    public User findUserByName(String name){
        return this.userRepo.UserByName(name);
    }

    @Transactional()
    public long saveDto(User userDto) {
        userDto.setPassword(bCryptPasswordEncoder
                .encode(userDto.getPassword()));
        return saveUser(userDto).getId();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new UtilisateurPrincipal(user);
    }
}
