package fr.cesi.ril19.madera.demo.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="produits")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Produit {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private double prix_total;
    private Date date_creation;
    private Date date_maj;
    private Boolean validation;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(name = "module_compose_produits",
            joinColumns = @JoinColumn(name = "module_client_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "produit_id",
                    referencedColumnName = "id"))
    private List<ModuleClient> moduleClients;

    @ManyToOne
    @JoinColumn(name="gamme_id")
    private Gamme gamme;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="devis_id")
    private Devis devis;
}
