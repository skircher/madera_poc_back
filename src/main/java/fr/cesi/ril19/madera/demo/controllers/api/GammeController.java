package fr.cesi.ril19.madera.demo.controllers.api;

import fr.cesi.ril19.madera.demo.entities.Gamme;
import fr.cesi.ril19.madera.demo.entities.ModuleClient;
import fr.cesi.ril19.madera.demo.services.GammeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/gamme")
public class GammeController {

    private GammeService gammeService;

    @Autowired
    GammeController( GammeService gammeService){
        this.gammeService = gammeService;
    }

    /**
     * GET /gamme
     * @return
     */
    @RequestMapping("")
    public List<Gamme> getGammes() {
        return this.gammeService.getGammes();
    }

    /**
     * POST /gamme
     * @param g
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Gamme> postGamme(@RequestBody Gamme g) {
        return new ResponseEntity<Gamme>(this.gammeService.saveGamme(g), HttpStatus.CREATED);
    }

    /**
     * POST /import
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<List<Gamme>> importGammes(@RequestParam("file") MultipartFile file) throws IOException {
        for (Gamme g:this.gammeService.importGamme(file)) {
            this.gammeService.saveGamme(g);
        }
        return new ResponseEntity<List<Gamme>>(this.gammeService.getGammes(), HttpStatus.CREATED);
    }

}
