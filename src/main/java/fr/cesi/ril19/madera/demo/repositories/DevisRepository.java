package fr.cesi.ril19.madera.demo.repositories;

import fr.cesi.ril19.madera.demo.entities.Devis;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DevisRepository extends CrudRepository<Devis, Long> {

    @Query(value="SELECT * FROM devis " +
            "WHERE client_id = :id"
            , nativeQuery = true)
    List<Devis> devisByClient(Long id);
}
