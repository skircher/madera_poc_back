package fr.cesi.ril19.madera.demo.controllers.api;


import fr.cesi.ril19.madera.demo.entities.Produit;
import fr.cesi.ril19.madera.demo.requestobject.RequestCreationProduit;
import fr.cesi.ril19.madera.demo.services.ModuleClientService;
import fr.cesi.ril19.madera.demo.services.ProduitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/produit")
public class ProduitController {

    private ProduitService produitService;
    private ModuleClientService moduleClientService;
    @Autowired
    ProduitController(ProduitService produitService, ModuleClientService moduleClientService){
        this.produitService = produitService;
        this.moduleClientService = moduleClientService;
    }

    /**
     * GET /produit
     * @return
     */
    @RequestMapping("")
    public List<Produit> getProduits() {
        return this.produitService.getProduits();
    }

    /**
     * GET /project/1 - /produit/id
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public Produit getProduitById(@PathVariable Long id) {
        return this.produitService.findProduitById(id);
    }

    /**
     * POST /produit
     * @param p
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Produit> postProduit(@RequestBody Produit p) {
        return new ResponseEntity<Produit>(this.produitService.saveProduit(p), HttpStatus.CREATED);
    }

    /**
     * PUT /produit
     * @param p
     * @return
     */
    @PutMapping("")
    public ResponseEntity<Produit> putProduit(@RequestBody Produit p) {
        return new ResponseEntity<Produit>(this.produitService.updateProduit(p), HttpStatus.CREATED);
    }

    /**
     * DELETE /produit
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deleteProduit(@PathVariable Long id) {
        return new ResponseEntity<Long>(this.produitService.deleteProduitById(id), HttpStatus.ACCEPTED);
    }

    /**
     * POST /import
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<List<Produit>> importProduits(@RequestParam("file") MultipartFile file) throws IOException {
        for (Produit p:this.produitService.importProduit(file)) {   //allows to identify if manager doesn't exist in table Users
            this.produitService.saveProduit(p);
        }
        return new ResponseEntity<List<Produit>>(this.produitService.getProduits(), HttpStatus.CREATED);
    }

    /**
     * POST /modulecomposeproduit
     * @param o
     * @return
     */
    @PostMapping("/modulecomposeproduit")
    public ResponseEntity<Produit> postModulesComposeProduit(@RequestBody RequestCreationProduit o) {
        return new ResponseEntity<Produit>(this.produitService.saveAllModules(this.moduleClientService.getModuleClientsByName(o.getNom()),o.getProduitId()), HttpStatus.CREATED);
    }
}


