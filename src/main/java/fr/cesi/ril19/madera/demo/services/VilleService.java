package fr.cesi.ril19.madera.demo.services;

import fr.cesi.ril19.madera.demo.entities.Ville;
import fr.cesi.ril19.madera.demo.repositories.VilleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VilleService {

    VilleRepository villeRepo;
    @Autowired
    VilleService(VilleRepository villeRepo) {
        this.villeRepo = villeRepo;
    }




    /**
     *
     * @return Liste des villes
     */
    public List<Ville> getVilles() {
        return (List<Ville>) this.villeRepo.findAll();
    }

    /**
     *
     * @param id
     * @return On ville thanks to an Id
     */
    public Ville findVilleById(Long id) { return this.villeRepo.findById(id).orElseThrow(); }

    public Ville saveVille(Ville v)  { return this.villeRepo.save(v);}


    /**
     * Import Project in csv file in src/main/resources/csv/projets.csv
     * @return List of Projet present in csv file
     * @throws IOException
     */
    public List<Ville> importVille(MultipartFile file) throws IOException {
        //String fileName = "src/main/resources/csv/projets.csv";
        String separator = ",";


        InputStream is = file.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<Ville> listV = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            String[] array = line.split(separator);
            Ville v = new Ville();
            //v.setId(Long.parseLong(array[0]));
            v.setNom(array[0]);
            v.setCode_postal(array[1]);
            v.setPays("France");


            listV.add(v);
        }return listV;
    }
}




