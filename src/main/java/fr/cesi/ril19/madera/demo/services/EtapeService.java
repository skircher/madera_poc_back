package fr.cesi.ril19.madera.demo.services;

import fr.cesi.ril19.madera.demo.entities.Etape;
import fr.cesi.ril19.madera.demo.repositories.EtapeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class EtapeService {
    EtapeRepository etapeRepo;

    @Autowired
    EtapeService(EtapeRepository etapeRepo){
        this.etapeRepo = etapeRepo;
    }

    /**
     *
     * @return list of etapes
     */
    public List<Etape> getEtapes() {
        return (List<Etape>) this.etapeRepo.findAll();
    }

    /**
     *
     * @param id
     * @return etape thanks to Id
     */
    public Etape findEtapeById(Long id) {
        return this.etapeRepo.findById(id).orElseThrow();
    }

    /**
     *
     * @param e
     * @return etape save
     */
    public Etape saveEtape(Etape e) {
        return this.etapeRepo.save(e);
    }

    /**
     * Import Etape in csv file
     * @param file csv
     * @return List of Etape present in csv file
     */
    public List<Etape> importEtape(MultipartFile file) throws IOException {
        String separator = ",";
        InputStream is = file.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<Etape> listE = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            String[] array = line.split(separator);
            Etape e = new Etape();
            //v.setId(Long.parseLong(array[0]));
            e.setNom(array[0]);
            listE.add(e);
        }return listE;
    }

}
