package fr.cesi.ril19.madera.demo.repositories;

import fr.cesi.ril19.madera.demo.entities.EtapeComposeDevis;
import org.springframework.data.repository.CrudRepository;

public interface EtapeComposeDevisRepository  extends CrudRepository<EtapeComposeDevis, Long> {
}
