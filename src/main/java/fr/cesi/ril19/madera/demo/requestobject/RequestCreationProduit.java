package fr.cesi.ril19.madera.demo.requestobject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestCreationProduit {
    private List<String> nom;
    private Long produitId;
}
