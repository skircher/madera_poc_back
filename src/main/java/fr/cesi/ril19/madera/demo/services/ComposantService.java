package fr.cesi.ril19.madera.demo.services;

import fr.cesi.ril19.madera.demo.entities.Composant;
import fr.cesi.ril19.madera.demo.repositories.ComposantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ComposantService {

    ComposantRepository composantRepo;

    @Autowired
    ComposantService(ComposantRepository composantRepo) {
        this.composantRepo = composantRepo;
    }

    /**
     *
     * @return Liste de Composants
     */
    public List<Composant> getComposants() {
        return (List<Composant>) this.composantRepo.findAll();
    }

    /**
     *
     * @param id
     * @return client grace à l'id du Composant
     */
    public Composant findComposantById(Long id) {
        return this.composantRepo.findById(id).orElseThrow();
    }

    /**
     *
     * @param c
     * @return Adresse save
     */
    public Composant saveComposant(Composant c) {
        return this.composantRepo.save(c);
    }

    public List<Composant> importComposant(MultipartFile file) throws IOException {
        String separator = ",";
        InputStream is = file.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<Composant> listC = new ArrayList<>();
        String line;
        double dimension[] = new double[3];
        while ((line = reader.readLine()) != null) {
            String[] array = line.split(separator);
            Composant c = new Composant();
            //v.setId(Long.parseLong(array[0]));
            c.setNom(array[0]);
            c.setDimension(dimension);
            c.setNature(array[1]);
            c.setFamille(array[2]);
            c.setQuantite_stock(Integer.parseInt(array[3]));
            c.setPrix(Integer.parseInt(array[4]));
            listC.add(c);
        }return listC;
    }
}
