package fr.cesi.ril19.madera.demo;

public enum Etats {
    Brouillon,
    Attente_client,
    En_commande,
    En_facturation
}
