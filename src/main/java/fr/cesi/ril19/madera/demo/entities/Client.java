package fr.cesi.ril19.madera.demo.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="clients")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Client {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String prenom;
    private String nom;
    @Column(unique=true)
    private String email;
    private String telephone_fixe;
    private String telephone_port;
    private Boolean genre;
    private Date date_creation;


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "situer_clients",
            joinColumns = @JoinColumn(name = "adresse_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "client_id",
                    referencedColumnName = "id"))
    private List<Adresse> adresses;

    //@JsonBackReference
    //@OneToMany(mappedBy = "client")
    //private List<Devis> devis;
}
