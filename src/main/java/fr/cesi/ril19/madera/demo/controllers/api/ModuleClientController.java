package fr.cesi.ril19.madera.demo.controllers.api;

import fr.cesi.ril19.madera.demo.entities.ModuleClient;
import fr.cesi.ril19.madera.demo.services.ModuleClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/moduleclient")
public class ModuleClientController {

    private ModuleClientService moduleclientService;

    @Autowired
    ModuleClientController(ModuleClientService moduleclientService){
        this.moduleclientService = moduleclientService;
    }

    /**
     * GET /module client
     * @return
     */
    @RequestMapping("")
    public List<ModuleClient> getModuleClients() {
        return this.moduleclientService.getModuleClients();
    }

    /**
     * GET /module client/1 - /moduleclient/id
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    @ResponseBody
    public ModuleClient getModuleClientById(@PathVariable Long id) {
        return this.moduleclientService.findModuleClientById(id);
    }

    /**
     * GET /module client/mur_droit - /moduleclient/nom
     * @param nom
     * @return
     */
    @RequestMapping("/nom/{nom}")
    @ResponseBody
    public ModuleClient getModuleClientByName(@PathVariable String nom) {
        return this.moduleclientService.findModuleClientByName(nom);
    }

    /**
     * POST /moduleclient
     * @param m
     * @return
     */
    @PostMapping("/moduleclient")
    public ResponseEntity<ModuleClient> postModuleClient(@RequestBody ModuleClient m) {
        return new ResponseEntity<ModuleClient>(this.moduleclientService.saveModuleClient(m), HttpStatus.CREATED);
    }

    /**
     * POST /import
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<List<ModuleClient>> importModuleClients(@RequestParam("file") MultipartFile file) throws IOException {
        for (ModuleClient m:this.moduleclientService.importModuleClient(file)) {   //allows to identify if manager doesn't exist in table Users
            this.moduleclientService.saveModuleClient(m);
        }
        return new ResponseEntity<List<ModuleClient>>(this.moduleclientService.getModuleClients(), HttpStatus.CREATED);
    }

}
