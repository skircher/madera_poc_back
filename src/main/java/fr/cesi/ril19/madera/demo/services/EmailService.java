package fr.cesi.ril19.madera.demo.services;

import com.itextpdf.text.BadElementException;
import fr.cesi.ril19.madera.demo.entities.Devis;
import fr.cesi.ril19.madera.demo.entities.Produit;
import fr.cesi.ril19.madera.demo.entities.User;
import fr.cesi.ril19.madera.demo.payload.GeneratePdfReport;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class EmailService {

    private final JavaMailSender mailSender;
    UserService userService;
    DevisService devisService;
    ProduitService produitService;
    GeneratePdfReport generatePdfReport;

    public EmailService(JavaMailSender mailSender, UserService userService, DevisService devisService,ProduitService produitService, GeneratePdfReport generatePdfReport) {
        this.mailSender = mailSender;
        this.userService = userService;
        this.devisService = devisService;
        this.generatePdfReport = generatePdfReport;
        this.produitService = produitService;
    }

    public void envoiCourrier(String username, Long devisId, Long produitId) throws MessagingException, IOException, BadElementException {
        User utilisateur = this.userService.findUserByName(username);
        Devis devis = this.devisService.findDevisById(devisId);
        Produit produitValide = this.produitService.findProduitById(produitId);

        MimeMessage messageToFacturation = this.mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(messageToFacturation, true);
        helper.setFrom(utilisateur.getEmail());
        helper.setTo("facturation@madera.com");
        helper.setText("<p>Bonjour,</p> <p>Vous trouverez en pièce jointe le dossier du devis n°: "+devis.getNumero_devis()+"</p> <p>Bien cordialement,</p> <p>"+utilisateur.getFirst_name()+ " " +utilisateur.getLast_name()+"</p>", true);
        helper.setSubject("Facturation devis n°: " + devis.getNumero_devis());
        File outputFileName = new File("filename.pdf");
        IOUtils.copy(this.generatePdfReport.docTechniquePdf(devis,produitValide.getId()), new FileOutputStream(outputFileName));

        helper.addAttachment("devis_"+devis.getNumero_devis()+".pdf", outputFileName);

        this.mailSender.send(messageToFacturation);
    }
}
