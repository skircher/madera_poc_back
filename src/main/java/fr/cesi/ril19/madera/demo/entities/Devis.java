package fr.cesi.ril19.madera.demo.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import fr.cesi.ril19.madera.demo.Etats;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="devis")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Devis {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private Integer numero_devis;
    private Date date_creation;
    private Date date_maj;
    @Enumerated(EnumType.ORDINAL)
    private Etats etat;

    @ManyToOne
    @JoinColumn(name="etape_id")
    private Etape etape;

    @OneToMany(mappedBy = "devis")
    private List<EtapeComposeDevis> etapeComposeDevis;

    @ManyToOne
    @JoinColumn(name="client_id")
    private Client client;

    @ManyToOne
    @JoinColumn(name="adresse_id")
    private Adresse adresse;


    @OneToMany(mappedBy = "devis")
    private List<Produit> produits;

}
