package fr.cesi.ril19.madera.demo.controllers.api;

import fr.cesi.ril19.madera.demo.entities.Adresse;
import fr.cesi.ril19.madera.demo.entities.Client;
import fr.cesi.ril19.madera.demo.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/client")
public class ClientController {
    private ClientService clientService;
    @Autowired
    ClientController(ClientService clientService){
        this.clientService = clientService;
    }

    /**
     * GET /client
     * @return
     */
    @RequestMapping("")
    public List<Client> getClients() {
        return this.clientService.getClients();
    }

    /**
     * GET /client/1 - /client/id
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public Client getClientById(@PathVariable Long id) {
        return this.clientService.findClientById(id);
    }

    /**
     * POST /client
     * @param c
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Client> postClient(@RequestBody Client c) {
        return new ResponseEntity<Client>(this.clientService.saveClient(c), HttpStatus.CREATED);
    }

    /**
     * POST /client/adresse/1
     * @param id, a
     * @return
     */
    @PostMapping("/adresse/{id}")
    public ResponseEntity<Client> postClientAdresse(@PathVariable Long id,@RequestBody Adresse a) {
        return new ResponseEntity<Client>(this.clientService.addAdresseClient(id,a), HttpStatus.CREATED);
    }

    /**
     * POST /import
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<List<Client>> importClients(@RequestParam("file") MultipartFile file) throws IOException {
        for (Client c:this.clientService.importClient(file)) {   //allows to identify if manager doesn't exist in table Users
            this.clientService.saveClient(c);
        }
        return new ResponseEntity<List<Client>>(this.clientService.getClients(), HttpStatus.CREATED);
    }
}
