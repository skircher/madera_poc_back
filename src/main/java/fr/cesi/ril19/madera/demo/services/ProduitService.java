package fr.cesi.ril19.madera.demo.services;

import fr.cesi.ril19.madera.demo.entities.ModuleClient;
import fr.cesi.ril19.madera.demo.entities.Produit;
import fr.cesi.ril19.madera.demo.repositories.ProduitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

@Service
public class ProduitService {

    ProduitRepository produitRepo;
    GammeService gammeService;
    DevisService devisService;
    ModuleClientService moduleClientService;

    @Autowired
    ProduitService(ProduitRepository produitRepo,GammeService gammeService,DevisService devisService, ModuleClientService moduleClientService){

        this.produitRepo = produitRepo;
        this.gammeService = gammeService;
        this.devisService = devisService;
        this.moduleClientService = moduleClientService;
    }

    /**
     *
     * @return list of produits
     */
    public List<Produit> getProduits() {
        return (List<Produit>) this.produitRepo.findAll();
    }

    /**
     *
     * @param id
     * @return produit thanks to Id
     */
    public Produit findProduitById(Long id) {
        return this.produitRepo.findById(id).orElseThrow();
    }

    /**
     *
     * @param id
     * @return produit thanks to Id
     */
    public Long deleteProduitById(Long id) {
        this.produitRepo.delete(this.produitRepo.findById(id).orElseThrow());
        return id;
    }

    /**
     *
     * @param p
     * @return project save
     */
    public Produit saveProduit(Produit p) {
        p.setDate_creation(new Date());
        p.setDate_maj(new Date());
        return this.produitRepo.save(p);
    }

    /**
     * allows to update a project or create if it doesn't exist
     * @param p Project in the requestbody
     * @return Projet modify or create
     */
    public Produit updateProduit(Produit p) {
        //create an instance of Produit with information in the Body Put
        Optional<Produit> updateProduit = produitRepo.findById(p.getId());
        if(updateProduit.isPresent())
        {
            Produit pPresent = updateProduit.get();
            if(p.getNom() != null)
                pPresent.setNom(p.getNom());
            if(p.getPrix_total() != 0)
                pPresent.setPrix_total(p.getPrix_total());
            if(p.getValidation())
                pPresent.setValidation(p.getValidation());
            pPresent.setDate_maj(new Date());
            pPresent = produitRepo.save(pPresent);
            return pPresent;
        }else  {
            p = produitRepo.save(p);
            return p;
        }
    }
    /**
     * Import Produit in csv file
     * @param file csv
     * @return List of Produit present in csv file
     */
    public List<Produit> importProduit(MultipartFile file) throws IOException {
        String separator = ",";
        InputStream is = file.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<Produit> listP = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            String[] array = line.split(separator);
            Produit p = new Produit();
            //v.setId(Long.parseLong(array[0]));
            p.setNom(array[0]);
            p.setGamme(this.gammeService.findGammeById(Long.parseLong(array[1])));
            p.setDevis(this.devisService.findDevisById(Long.parseLong(array[2])));
            p.setValidation(false);
            listP.add(p);
        }return listP;
    }

    /**
     *
     * @param m
     * @return ModuleComposeProduit save
     */
    public Produit saveAllModules(List<ModuleClient> m, Long produitId) {

        Produit produitUp = this.findProduitById(produitId);
        double prixMaison = 0;

        List<ModuleClient> listMclient = new ArrayList<>();
        for (ModuleClient module_clt:
                m) {
            listMclient.add(module_clt);
            prixMaison = prixMaison + this.moduleClientService.getPrixModule(module_clt.getId());
        }
        produitUp.setModuleClients(listMclient);
        produitUp.setPrix_total(prixMaison);
        produitUp.setDate_maj(new Date());
        this.updateProduit(produitUp);

        return produitUp;
    }

}
