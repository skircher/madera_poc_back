package fr.cesi.ril19.madera.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="module_clients")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModuleClient {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String nom;

    @ManyToOne
    @JoinColumn(name="famille_module_id")
    private FamilleModule familleModule;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "composant_compose_modules",
            joinColumns = @JoinColumn(name = "composant_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "module_client_id",
                    referencedColumnName = "id"))
    private List<Composant> composants;


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "gamme_modules",
            joinColumns = @JoinColumn(name = "gamme_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "module_client_id",
                    referencedColumnName = "id"))
    private List<Gamme> Gammes;
}
