package fr.cesi.ril19.madera.demo.services;

import fr.cesi.ril19.madera.demo.Etats;
import fr.cesi.ril19.madera.demo.entities.Devis;
import fr.cesi.ril19.madera.demo.repositories.DevisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class DevisService {
    DevisRepository devisRepo;
    ClientService clientService;
    AdresseService adresseService;

    @Autowired
    DevisService(DevisRepository devisRepo,ClientService clientService,AdresseService adresseService)
    {
        this.devisRepo = devisRepo;
        this.clientService = clientService;
        this.adresseService = adresseService;
    }

    /**
     *
     * @return list of deviss
     */
    public List<Devis> getDeviss() {
        return (List<Devis>) this.devisRepo.findAll();
    }

    /**
     *
     * @param id
     * @return project thanks to Id
     */
    public Devis findDevisById(Long id) {
        return this.devisRepo.findById(id).orElseThrow();
    }

    /**
     *
     * @param g
     * @return devis save
     */
    public Devis saveDevis(Devis g) {
        g.setDate_creation(new Date());
        g.setDate_maj(new Date());
        return this.devisRepo.save(g);
    }

    /**
     * Permet de mettre à jour le devis
     * @param d Devis dans le requestbody
     * @return Devis
     */
    public Devis updateDevis(Devis d) {
        Optional<Devis> updateDevis = devisRepo.findById(d.getId());
        if(updateDevis.isPresent())
        {
            Devis dPresent = updateDevis.get();
            if(d.getEtat() != null)
                dPresent.setEtat(d.getEtat());

            if(d.getEtape() != null)
                dPresent.setEtape(d.getEtape());

            dPresent.setDate_maj(new Date());
            dPresent = devisRepo.save(dPresent);
            return dPresent;
        }else  {
            d = devisRepo.save(d);
            return d;
        }
    }

    /**
     * Import Devis in csv file
     * @param file csv
     * @return List of Devis present in csv file
     */
    public List<Devis> importDevis(MultipartFile file) throws IOException {
        String separator = ",";
        InputStream is = file.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<Devis> listG = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            String[] array = line.split(separator);
            Devis g = new Devis();
            //v.setId(Long.parseLong(array[0]));
            g.setNumero_devis(Integer.parseInt(array[0]));
            g.setClient(this.clientService.findClientById(Long.parseLong(array[1])));
            g.setAdresse(this.adresseService.findAdresseById(Long.parseLong(array[2])));
            g.setEtat(Etats.valueOf(array[3]));
            listG.add(g);
        }return listG;
    }

    /**
     *
     * @param client_id
     * @return list of devis thanks to Id
     */
    public List<Devis> getDevisByClient(Long client_id) {
        return this.devisRepo.devisByClient(client_id);
    }
}
