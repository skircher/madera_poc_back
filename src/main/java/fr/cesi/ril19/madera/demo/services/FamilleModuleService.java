package fr.cesi.ril19.madera.demo.services;

import fr.cesi.ril19.madera.demo.entities.FamilleModule;
import fr.cesi.ril19.madera.demo.entities.Gamme;
import fr.cesi.ril19.madera.demo.repositories.FamilleModuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class FamilleModuleService {
    FamilleModuleRepository familleModuleRepo;

    @Autowired
    FamilleModuleService(FamilleModuleRepository familleModuleRepo){
        this.familleModuleRepo = familleModuleRepo;
    }

    /**
     *
     * @return list of familleModules
     */
    public List<FamilleModule> getFamilleModules() {
        return (List<FamilleModule>) this.familleModuleRepo.findAll();
    }

    /**
     *
     * @param id
     * @return FamilleModule thanks to Id
     */
    public FamilleModule findFamilleModuleById(Long id) {
        return this.familleModuleRepo.findById(id).orElseThrow();
    }

    /**
     *
     * @param g
     * @return familleModule save
     */
    public FamilleModule saveFamilleModule(FamilleModule g) {
        return this.familleModuleRepo.save(g);
    }

    /**
     * Import FamilleModule in csv file
     * @param file csv
     * @return List of FamilleModule present in csv file
     */
    public List<FamilleModule> importFamilleModule(MultipartFile file) throws IOException {
        String separator = ",";
        InputStream is = file.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<FamilleModule> listG = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            String[] array = line.split(separator);
            FamilleModule g = new FamilleModule();
            //v.setId(Long.parseLong(array[0]));
            g.setNom(array[0]);
            listG.add(g);
        }return listG;
    }

}
