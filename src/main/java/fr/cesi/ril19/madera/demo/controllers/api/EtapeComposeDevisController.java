package fr.cesi.ril19.madera.demo.controllers.api;

import fr.cesi.ril19.madera.demo.entities.EtapeComposeDevis;
import fr.cesi.ril19.madera.demo.entities.FamilleModule;
import fr.cesi.ril19.madera.demo.services.EtapeComposeDevisService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/etapeDevis")
public class EtapeComposeDevisController {

    private EtapeComposeDevisService etapeComposeDevisService;

    EtapeComposeDevisController(EtapeComposeDevisService etapeComposeDevisService){
        this.etapeComposeDevisService = etapeComposeDevisService;
    }

    /**
     * POST /etapeDevis
     * @param e
     * @return
     */
    @PostMapping("")
    public ResponseEntity<EtapeComposeDevis> postEtapeComposeDevis(@RequestBody EtapeComposeDevis e) {
        return new ResponseEntity<EtapeComposeDevis>(this.etapeComposeDevisService.saveEtapeComposeDevis(e), HttpStatus.CREATED);
    }

}
