package fr.cesi.ril19.madera.demo.services;

import fr.cesi.ril19.madera.demo.entities.EtapeComposeDevis;
import fr.cesi.ril19.madera.demo.repositories.EtapeComposeDevisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EtapeComposeDevisService {

    EtapeComposeDevisRepository etapeComposeDevisRepository;

    @Autowired
    EtapeComposeDevisService(EtapeComposeDevisRepository etapeComposeDevisRepository){
        this.etapeComposeDevisRepository = etapeComposeDevisRepository;
    }

    /**
     *
     * @param e
     * @return etapeComposeDevis save
     */
    public EtapeComposeDevis saveEtapeComposeDevis(EtapeComposeDevis e) {
        return this.etapeComposeDevisRepository.save(e);
    }

}
