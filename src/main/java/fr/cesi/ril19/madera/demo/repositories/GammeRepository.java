package fr.cesi.ril19.madera.demo.repositories;


import fr.cesi.ril19.madera.demo.entities.Gamme;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GammeRepository extends CrudRepository<Gamme, Long> {
}
