package fr.cesi.ril19.madera.demo.payload;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import fr.cesi.ril19.madera.demo.entities.*;
import fr.cesi.ril19.madera.demo.services.FileStorageService;
import fr.cesi.ril19.madera.demo.services.ModuleClientService;
import fr.cesi.ril19.madera.demo.services.ProduitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.List;

@Service
public class GeneratePdfReport {

    ModuleClientService moduleClientService;
    ProduitService produitService;
    FileStorageService fileStorageService;

    @Autowired
    public GeneratePdfReport(ModuleClientService moduleClientService, ProduitService produitService, FileStorageService fileStorageService){
        this.moduleClientService = moduleClientService;
        this.produitService = produitService;
        this.fileStorageService = fileStorageService;
    }

    private static final Logger logger = LoggerFactory.getLogger(GeneratePdfReport.class);

    public ByteArrayInputStream docTechniquePdf(Devis devis, Long id_produit) throws IOException, BadElementException {

        /* Configuration des dimensions du document */

        float left = 30;
        float right = 30;
        float top = 20;
        float bottom = 20;

        Document document = new Document();
        document.setMargins(left, right, top, bottom);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        /*Import Logo entreprise + position*/

        String imageUrl = "src/main/resources/img/logo_madera.png";
        Image logo = Image.getInstance(imageUrl);
        logo.setAbsolutePosition(400, 750);


        /*Import image vue trigonométrique*/
        Image planIsometrique = Image.getInstance(this.fileStorageService.loadFileAsResource(id_produit+"_vue_trigo.jpg").getURL());
        planIsometrique.scalePercent(50);

        /*Import image vue coupe x*/
        Image coupeX = Image.getInstance(this.fileStorageService.loadFileAsResource(id_produit+"_vue_axe_x.jpg").getURL());
        coupeX.scalePercent(50);

        /*Import image vue coupe y*/
        Image coupeY = Image.getInstance(this.fileStorageService.loadFileAsResource(id_produit+"_vue_axe_y.jpg").getURL());
        coupeY.scalePercent(50);

        /*Import image vue coupe z*/
        Image coupeZ = Image.getInstance(this.fileStorageService.loadFileAsResource(id_produit+"_vue_axe_z.jpg").getURL());
        coupeZ.scalePercent(50);


        /*Configuration des Font du document*/

        Font titleFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        titleFont.setSize(20);

        Font nameFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        nameFont.setSize(15);

        Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        headFont.setSize(15);

        Font moduleFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        moduleFont.setSize(10);

        Font composantFont = FontFactory.getFont(FontFactory.HELVETICA);
        composantFont.setSize(8);

        /*Configuration Section entête*/


        Paragraph nomPrenomParagraphe = new Paragraph(devis.getClient().getNom() + " " + devis.getClient().getPrenom(),nameFont);
        Paragraph adresseParagraphe = new Paragraph((devis.getClient().getAdresses().get(0).getNumero() + " " + devis.getClient().getAdresses().get(0).getRue()));
        Paragraph villeParagraphe = new Paragraph((devis.getClient().getAdresses().get(0).getVille().getCode_postal() + " " + devis.getClient().getAdresses().get(0).getVille().getNom()));
        villeParagraphe.setSpacingAfter(50);

        Paragraph titleParagraph = new Paragraph("Plans et vues de la maison:",titleFont);

        Paragraph paragraphVueIso = new Paragraph("Vue isométrique:",moduleFont);

        Paragraph paragraphPlanX = new Paragraph("Plan de coupe X:",moduleFont);
        Paragraph paragraphPlanY = new Paragraph("Plan de coupe Y:",moduleFont);
        Paragraph paragraphPlanZ = new Paragraph("Plan de coupe Z:",moduleFont);


        Paragraph echelonParagraph = new Paragraph("Echelonnements devis n° :" + devis.getNumero_devis(),titleFont);


        try {

            PdfPTable table = new PdfPTable(4);
            table.setWidthPercentage(90);
            table.setHorizontalAlignment(5);
            table.setWidths(new int[]{7, 10, 3, 3});
            table.setSpacingBefore(10f);

            PdfPCell hcell;

            hcell = new PdfPCell(new Phrase("Famille", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            //hcell.setBackgroundColor();
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Nom", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Qté", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Prix", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            List<Produit> produits = devis.getProduits();

            Double prixTotalProduit = this.produitService.findProduitById(id_produit).getPrix_total();
            //int id_produit = 0 ;


            Map<ModuleClient, Integer> mapModule= new HashMap<ModuleClient, Integer>();
            List<ModuleClient> moduleProduit = this.produitService.findProduitById(id_produit).getModuleClients();

            for (ModuleClient module : moduleProduit){
                mapModule.put(module,Collections.frequency(moduleProduit,module));
            }

            for (Map.Entry<ModuleClient,Integer> module : mapModule.entrySet()) {

                PdfPCell cell;

                cell = new PdfPCell(new Phrase(module.getKey().getFamilleModule().getNom(),moduleFont));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(module.getKey().getNom().replace("_"," "),moduleFont));
                cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(module.getValue().toString(),moduleFont));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setPaddingRight(5);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(this.moduleClientService.getPrixModule(module.getKey().getId())+"€",moduleFont));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setPaddingRight(5);
                table.addCell(cell);

                Set<Composant> listeComposant = new HashSet<Composant>(module.getKey().getComposants());

                Map<Composant, Integer> mapComposant= new HashMap<Composant, Integer>();
                List<Composant> compoasntModule = module.getKey().getComposants();

                for (Composant composant : module.getKey().getComposants()){
                    mapComposant.put(composant,Collections.frequency(compoasntModule,composant));
                }

                for (Map.Entry<Composant,Integer> c : mapComposant.entrySet()){

                    cell = new PdfPCell(new Phrase(c.getKey().getFamille(),composantFont));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(c.getKey().getNom().replace("_"," "),composantFont));
                    cell.setPaddingLeft(5);
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(c.getValue().toString(),composantFont));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setPaddingRight(5);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(c.getKey().getPrix()+"€",composantFont));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setPaddingRight(5);
                    table.addCell(cell);

                }
            }
            PdfPTable tableEchelon = new PdfPTable(3);
            tableEchelon.setWidthPercentage(90);
            tableEchelon.setHorizontalAlignment(5);
            tableEchelon.setWidths(new int[]{7, 4, 4});
            tableEchelon.setSpacingBefore(10f);

            PdfPCell hcellEchelon;

            hcellEchelon = new PdfPCell(new Phrase("Etape", headFont));
            hcellEchelon.setHorizontalAlignment(Element.ALIGN_CENTER);
            //hcell.setBackgroundColor();
            tableEchelon.addCell(hcellEchelon);

            hcellEchelon = new PdfPCell(new Phrase("Pourcentage", headFont));
            hcellEchelon.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableEchelon.addCell(hcellEchelon);

            hcellEchelon = new PdfPCell(new Phrase("Montant", headFont));
            hcellEchelon.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableEchelon.addCell(hcellEchelon);


            List<EtapeComposeDevis> etapesComposeDevis = devis.getEtapeComposeDevis();

            for (EtapeComposeDevis etape  : etapesComposeDevis) {

                PdfPCell cellEchelon;

                cellEchelon = new PdfPCell(new Phrase(etape.getEtape().getNom(),moduleFont));
                cellEchelon.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cellEchelon.setHorizontalAlignment(Element.ALIGN_LEFT);
                tableEchelon.addCell(cellEchelon);

                cellEchelon = new PdfPCell(new Phrase(etape.getPourcentage()+" %",moduleFont));
                cellEchelon.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cellEchelon.setHorizontalAlignment(Element.ALIGN_CENTER);
                cellEchelon.setPaddingRight(5);
                tableEchelon.addCell(cellEchelon);

                Double coutEtape = prixTotalProduit * etape.getPourcentage() / 100 ;

                cellEchelon = new PdfPCell(new Phrase(coutEtape + " €",moduleFont));
                cellEchelon.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cellEchelon.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cellEchelon.setPaddingRight(5);
                tableEchelon.addCell(cellEchelon);
            }

            PdfWriter.getInstance(document, out);
            document.open();

            document.add(logo);
            document.add(nomPrenomParagraphe);
            document.add(adresseParagraphe);
            document.add(villeParagraphe);
            document.add(new Paragraph("Date : " + devis.getDate_maj().toString().split(" ")[0]));

            document.add(new Paragraph("Devis N°: " + devis.getNumero_devis() + "    " + "Nom Maison : " + this.produitService.findProduitById(id_produit).getNom() + "    " + "Gamme : " + this.produitService.findProduitById(id_produit).getGamme().getNom()));

            document.add(table);
            document.add(new Paragraph("Prix Total : " + prixTotalProduit + "€"));

            document.add(titleParagraph);
            document.add(paragraphVueIso);
            document.add(planIsometrique);
            document.add(paragraphPlanY);
            document.add(coupeY);
            document.add(paragraphPlanX);
            document.add(coupeX);
            document.add(paragraphPlanZ);
            document.add(coupeZ);

            if (etapesComposeDevis.isEmpty() == false){
                document.add(echelonParagraph);
                document.add(tableEchelon);
            }



            document.close();

        } catch (DocumentException ex) {

            logger.error("Error occurred: {0}", ex);
        }
        return new ByteArrayInputStream(out.toByteArray());
    }
}
