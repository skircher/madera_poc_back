package fr.cesi.ril19.madera.demo.controllers.api;

import fr.cesi.ril19.madera.demo.entities.FamilleModule;
import fr.cesi.ril19.madera.demo.services.FamilleModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/famillemodule")
public class FamilleModuleController {
    private FamilleModuleService familleModuleService;

    @Autowired
    FamilleModuleController( FamilleModuleService familleModuleService){
        this.familleModuleService = familleModuleService;
    }

    /**
     * GET /familleModule
     * @return
     */
    @RequestMapping("")
    public List<FamilleModule> getFamilleModules() {
        return this.familleModuleService.getFamilleModules();
    }

    /**
     * POST /familleModule
     * @param g
     * @return
     */
    @PostMapping("")
    public ResponseEntity<FamilleModule> postFamilleModule(@RequestBody FamilleModule g) {
        return new ResponseEntity<FamilleModule>(this.familleModuleService.saveFamilleModule(g), HttpStatus.CREATED);
    }

    /**
     * POST /import
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<List<FamilleModule>> importFamilleModules(@RequestParam("file") MultipartFile file) throws IOException {
        for (FamilleModule g:this.familleModuleService.importFamilleModule(file)) {
            this.familleModuleService.saveFamilleModule(g);
        }
        return new ResponseEntity<List<FamilleModule>>(this.familleModuleService.getFamilleModules(), HttpStatus.CREATED);
    }
}
